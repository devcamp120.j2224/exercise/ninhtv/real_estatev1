package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IDesignUnitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DesignUnitService {
    @Autowired
    IDesignUnitRepository pDesignUnitRepository;

    public List<CDesignUnit> getAllDesignUnit() {
        List<CDesignUnit> designUnitList = new ArrayList<CDesignUnit>();
        pDesignUnitRepository.findAll().forEach(designUnitList::add);
        return designUnitList;
    }

    public Object createDesignUnitObj(CDesignUnit cDesignUnit) {
        CDesignUnit newDesignUnit = new CDesignUnit();

        newDesignUnit.setName(cDesignUnit.getName());
        newDesignUnit.setDescription(cDesignUnit.getDescription());
        newDesignUnit.setProjects(cDesignUnit.getProjects());
        newDesignUnit.setAddress(cDesignUnit.getAddress());
        newDesignUnit.setPhone(cDesignUnit.getPhone());
        newDesignUnit.setPhone2(cDesignUnit.getPhone2());
        newDesignUnit.setFax(cDesignUnit.getFax());
        newDesignUnit.setEmail(cDesignUnit.getEmail());
        newDesignUnit.setWebsite(cDesignUnit.getWebsite());
        newDesignUnit.setNote(cDesignUnit.getNote());

        CDesignUnit savedDesignUnit = pDesignUnitRepository.save(newDesignUnit);
        return savedDesignUnit;
    }

    public Object updateDesignUnitObj(CDesignUnit cDesignUnit, Optional<CDesignUnit> designUnitData) {
        CDesignUnit newDesignUnit = designUnitData.get();

        newDesignUnit.setName(cDesignUnit.getName());
        newDesignUnit.setDescription(cDesignUnit.getDescription());
        newDesignUnit.setProjects(cDesignUnit.getProjects());
        newDesignUnit.setAddress(cDesignUnit.getAddress());
        newDesignUnit.setPhone(cDesignUnit.getPhone());
        newDesignUnit.setPhone2(cDesignUnit.getPhone2());
        newDesignUnit.setFax(cDesignUnit.getFax());
        newDesignUnit.setEmail(cDesignUnit.getEmail());
        newDesignUnit.setWebsite(cDesignUnit.getWebsite());
        newDesignUnit.setNote(cDesignUnit.getNote());

        CDesignUnit savedDesignUnit = pDesignUnitRepository.save(newDesignUnit);
        return savedDesignUnit;
    }
}
