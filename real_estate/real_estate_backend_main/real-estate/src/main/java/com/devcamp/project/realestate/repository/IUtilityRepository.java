package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.project.realestate.model.CUtility;
import org.springframework.stereotype.Repository;

@Repository
public interface IUtilityRepository extends JpaRepository<CUtility, Integer> {

}
