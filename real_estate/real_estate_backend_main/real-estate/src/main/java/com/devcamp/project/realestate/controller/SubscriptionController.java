package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.SubscriptionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class SubscriptionController {
    @Autowired
    ISubscriptionRepository pSubscriptionRepository;

    @Autowired
    SubscriptionService pSubscriptionService;

    @GetMapping("/subscriptions")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getSubscriptionList() {
        if (!pSubscriptionService.getSubscriptionList().isEmpty()) {
            return new ResponseEntity<>(pSubscriptionService.getSubscriptionList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/subscriptions/{subscriptionId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getSubscriptionById(@PathVariable Integer subscriptionId) {
        if (pSubscriptionRepository.findById(subscriptionId).isPresent()) {
            return new ResponseEntity<>(pSubscriptionRepository.findById(subscriptionId).get(),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/subscriptions")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createSubscription(@RequestBody CSubscription cSubscription) {
        try {
            return new ResponseEntity<>(pSubscriptionService.createSubscriptionObj(cSubscription), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Subscription: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/subscriptions/{subscriptionId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateSubscription(@PathVariable("subscriptionId") Integer subscriptionId,
            @RequestBody CSubscription cSubscription) {
        try {
            Optional<CSubscription> subscriptionData = pSubscriptionRepository.findById(subscriptionId);
            if (subscriptionData.isPresent()) {
                return new ResponseEntity<>(pSubscriptionService.updateSubscriptionObj(cSubscription, subscriptionData),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Subscription: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/subscriptions/{subscriptionId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteSubscriptionById(@PathVariable("subscriptionId") int subscriptionId) {
        try {
            pSubscriptionRepository.deleteById(subscriptionId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
