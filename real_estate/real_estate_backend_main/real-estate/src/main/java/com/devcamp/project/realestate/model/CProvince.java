package com.devcamp.project.realestate.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "province")
public class CProvince {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "_code")
    private String code;

    @Column(name = "_name")
    private String name;

    @OneToMany(targetEntity = CDistrict.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "_province_id")
    @JsonIgnore
    private Set<CDistrict> districts;

    @OneToMany(targetEntity = CWard.class, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "_province_id")
    private Set<CWard> wards;

    @OneToMany(targetEntity = CStreet.class, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "_province_id")
    private Set<CStreet> streets;

    @OneToMany(targetEntity = CProject.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "_province_id")
    @JsonIgnore
    private Set<CProject> projects;

    @OneToMany(targetEntity = CRealEstate.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "province_id")
    @JsonIgnore
    private Set<CRealEstate> realEstates;

    public CProvince() {
    }

    public CProvince(int id, String code, String name, Set<CDistrict> districts) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.districts = districts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CDistrict> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }

    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }

    public Set<CStreet> getStreets() {
        return streets;
    }

    public void setStreets(Set<CStreet> streets) {
        this.streets = streets;
    }

    public Set<CProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<CProject> projects) {
        this.projects = projects;
    }

    public Set<CRealEstate> getRealEstates() {
        return realEstates;
    }

    public void setRealEstates(Set<CRealEstate> realEstates) {
        this.realEstates = realEstates;
    }

}
