package com.devcamp.project.realestate.service;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.IUtilityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilityService {
    @Autowired
    IUtilityRepository pUtilityRepository;

    public List<CUtility> getUtilityList() {
        List<CUtility> utilityList = new ArrayList<CUtility>();
        pUtilityRepository.findAll().forEach(utilityList::add);
        return utilityList;
    }

    public Object createUtilityObj(CUtility cUtility) {

        CUtility newUtility = new CUtility();
        newUtility.setName(cUtility.getName());
        newUtility.setDescription(cUtility.getDescription());
        newUtility.setPhoto(cUtility.getPhoto());

        CUtility savedUtility = pUtilityRepository.save(newUtility);
        return savedUtility;
    }

    public Object updateUtilityObj(CUtility cUtility, Optional<CUtility> cUtilityData) {

        CUtility newUtility = cUtilityData.get();
        newUtility.setName(cUtility.getName());
        newUtility.setDescription(cUtility.getDescription());
        newUtility.setPhoto(cUtility.getPhoto());

        CUtility savedUtility = pUtilityRepository.save(newUtility);
        return savedUtility;
    }
}
