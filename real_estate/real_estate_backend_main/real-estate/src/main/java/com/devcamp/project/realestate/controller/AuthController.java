package com.devcamp.project.realestate.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.project.realestate.entity.Role;
import com.devcamp.project.realestate.entity.Token;
import com.devcamp.project.realestate.model.CEmployees;
import com.devcamp.project.realestate.repository.RoleRepository;
import com.devcamp.project.realestate.security.JwtUtil;
import com.devcamp.project.realestate.security.UserPrincipal;
import com.devcamp.project.realestate.service.TokenService;
import com.devcamp.project.realestate.service.UserService;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    RoleRepository pRoleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/register")
    public CEmployees register(@RequestBody CEmployees user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        Optional<Role> roleData = pRoleRepository.findById(4);
        Role _role = roleData.get();
        Set<Role> set = new HashSet<Role>();
        set.add(_role);

        user.setRoles(set);
        user.setFirstName(user.getFirstName());
        user.setLastName(user.getLastName());
        user.setAddress(user.getAddress());
        user.setEmail(user.getEmail());

        return userService.createCEmployees(user);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody CEmployees user) {
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("tài khoản hoặc mật khẩu không chính xác");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        return ResponseEntity.ok(token.getToken());
    }

    @GetMapping("/findUserId/Token")
    public Integer getUserDataByToken() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Integer userId = ((UserPrincipal) userDetails).getUserId();
        return userId;
    }
}
