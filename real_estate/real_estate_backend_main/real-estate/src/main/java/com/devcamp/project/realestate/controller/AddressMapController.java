package com.devcamp.project.realestate.controller;

import java.util.*;

import com.devcamp.project.realestate.model.*;
import com.devcamp.project.realestate.repository.*;
import com.devcamp.project.realestate.service.AddressMapService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class AddressMapController {
    @Autowired
    IAddressMapRepository pAddressMapRepository;

    @Autowired
    AddressMapService pAddressMapService;

    @GetMapping("/addressMaps")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getAddressMapList() {
        if (!pAddressMapService.getAddressMapList().isEmpty()) {
            return new ResponseEntity<>(pAddressMapService.getAddressMapList(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/addressMaps/{addressMapId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> getAddressMapById(@PathVariable Integer addressMapId) {
        if (pAddressMapRepository.findById(addressMapId).isPresent()) {
            return new ResponseEntity<>(pAddressMapRepository.findById(addressMapId).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addressMaps")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> createAddressMap(@RequestBody CAddressMap cAddressMap) {
        try {
            return new ResponseEntity<>(pAddressMapService.createAddressMapObj(cAddressMap), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified AddressMap: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/addressMaps/{addressMapId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> updateAddressMap(@PathVariable("addressMapId") Integer addressMapId,
            @RequestBody CAddressMap cAddressMap) {
        try {
            Optional<CAddressMap> addressMapData = pAddressMapRepository.findById(addressMapId);
            if (addressMapData.isPresent()) {
                return new ResponseEntity<>(pAddressMapService.updateAddressMapObj(cAddressMap, addressMapData),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified AddressMap: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>("AddressMap NOT FOUND!", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/addressMaps/{addressMapId}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Object> deleteAddressMapById(@PathVariable Integer addressMapId) {
        try {
            pAddressMapRepository.deleteById(addressMapId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
