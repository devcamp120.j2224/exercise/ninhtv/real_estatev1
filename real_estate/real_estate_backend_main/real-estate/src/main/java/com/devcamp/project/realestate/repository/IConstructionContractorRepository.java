package com.devcamp.project.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.project.realestate.model.CConstructionContractor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface IConstructionContractorRepository extends JpaRepository<CConstructionContractor, Integer> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM `construction_contractor` WHERE `id` = :constructionContractorId ;", nativeQuery = true)
    Object deleteConstructionContractorById(@Param("constructionContractorId") int constructionContractorId);

}
